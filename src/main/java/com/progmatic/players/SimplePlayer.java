
package com.progmatic.players;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import com.progmatic.main.ImplementBoard;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimplePlayer extends AbstractPlayer{

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                try {
                    if(b.getCell(i, j).equals(PlayerType.EMPTY)){
                        Cell cell = new Cell(i, j, myType); 
                        return cell;
                    }
                } catch (CellException ex) {
                    Logger.getLogger(SimplePlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    
    
}
