package com.progmatic.players;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VictoryAwarePlayer extends AbstractPlayer {

    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        try{
            if(b.getCell(0, 0).equals(myType)
                    && b.getCell(1, 1).equals(myType)
                    && b.getCell(2, 2).equals(PlayerType.EMPTY)){
                Cell cell = new Cell(2,2,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            if(b.getCell(0, 0).equals(myType)
                    && b.getCell(1, 0).equals(myType)
                    && b.getCell(2, 0).equals(PlayerType.EMPTY)){
                Cell cell = new Cell(2,0,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            if(b.getCell(0, 1).equals(myType)
                    && b.getCell(1, 1).equals(myType)
                    && b.getCell(2, 1).equals(PlayerType.EMPTY)){
                Cell cell = new Cell(2,1,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            if(b.getCell(0, 2).equals(myType)
                    && b.getCell(1, 2).equals(myType)
                    && b.getCell(2, 2).equals(PlayerType.EMPTY)){
                Cell cell = new Cell(2,2,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            if(b.getCell(0, 2).equals(myType)
                    && b.getCell(1, 1).equals(myType)
                    && b.getCell(2, 0).equals(PlayerType.EMPTY)){
                Cell cell = new Cell(2,0,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }

        try{
            if(b.getCell(0, 2).equals(myType)
                    && b.getCell(1, 2).equals(PlayerType.EMPTY)
                    && b.getCell(2, 2).equals(myType)){
                Cell cell = new Cell(1,2,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            if(b.getCell(0, 1).equals(myType)
                    && b.getCell(1, 1).equals(PlayerType.EMPTY)
                    && b.getCell(2, 1).equals(myType)){
                Cell cell = new Cell(1,1,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            if(b.getCell(0, 0).equals(myType)
                    && b.getCell(1, 0).equals(PlayerType.EMPTY)
                    && b.getCell(2, 0).equals(myType)){
                Cell cell = new Cell(1,0,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            if(b.getCell(0, 0).equals(PlayerType.EMPTY)
                    && b.getCell(0, 1).equals(myType)
                    && b.getCell(0, 2).equals(myType)){
                Cell cell = new Cell(0,0,myType);
                return cell;
            }
        }catch(CellException ex){
            Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                try {
                    if(b.getCell(i, j).equals(PlayerType.EMPTY)){
                        Cell cell = new Cell(i, j, myType); 
                        return cell;
                    }
                } catch (CellException ex) {
                    Logger.getLogger(SimplePlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

}
