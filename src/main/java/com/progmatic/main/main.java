package com.progmatic.main;

import com.progmatic.players.SimplePlayer;
import com.progmatic.players.VictoryAwarePlayer;
import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;

public class main {

    public static void main(String[] args) throws CellException {
        TicTacToe_Game tcg = new TicTacToe_Game();
        tcg.game();
        
    }
    
}
