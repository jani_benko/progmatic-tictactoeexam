package com.progmatic.main;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

public class ImplementBoard implements Board {
    
    public static final int ROW_COUNT = 3;
    public static final int COL_NUMBER = 3;
    public PlayerType[][] listOfCells = new PlayerType[ROW_COUNT][COL_NUMBER];

    public ImplementBoard() {
        for (int i = 0; i < ROW_COUNT; i++) {
            for (int j = 0; j < COL_NUMBER; j++) {
                listOfCells[i][j] = PlayerType.EMPTY;
            }
        };
    }
    
    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        try {
            return listOfCells[rowIdx][colIdx];
        } catch (IndexOutOfBoundsException ex) {
            String message = "Index can't reach";
            throw new CellException(rowIdx, colIdx, message);
        }
    }

    @Override
    public void put(Cell cell) throws CellException {
        try {
            if (listOfCells[cell.getRow()][cell.getCol()].equals(PlayerType.EMPTY)) {
                listOfCells[cell.getRow()][cell.getCol()] = cell.getCellsPlayer();
            } else {
                String message = "The current cell is not empty";
                throw new CellException(cell.getRow(), cell.getCol(), message);
            }
        } catch (IndexOutOfBoundsException ex) {
            String message = "Index can't reach";
            throw new CellException(cell.getRow(), cell.getCol(), message);
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {
        int counter = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(listOfCells[i][j].equals(p)){
                    counter++;
                }else{
                    counter=0;
                    break;
                }
            }
            if(counter == 3){
                return true;
            }
        }
        if(listOfCells[0][0].equals(p) 
                && listOfCells[1][1].equals(p)
                && listOfCells[2][2].equals(p) ){
            return true;
        }
        if(listOfCells[0][2].equals(p) 
                && listOfCells[1][1].equals(p)
                && listOfCells[2][0].equals(p) ){
            return true;
        }
        for (int i = 0; i < 3; i++) {
            if(listOfCells[0][i].equals(p)){
                if(listOfCells[1][i].equals(p)){
                    if(listOfCells[2][i].equals(p)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    @Override
    public List<Cell> emptyCells() {
        List<Cell> empties = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(listOfCells[i][j].equals(PlayerType.EMPTY)){
                    Cell cell = new Cell(i,j,listOfCells[i][j]);
                    empties.add(cell);
                }
            }
        }
        return empties;
    }

}
