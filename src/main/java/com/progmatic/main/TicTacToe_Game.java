
package com.progmatic.main;

import com.progmatic.players.SimplePlayer;
import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.Scanner;

public class TicTacToe_Game {
    
    private ImplementBoard board;
    
    public void game() throws CellException{
        board = new ImplementBoard();
        while(!board.hasWon(PlayerType.O)||!board.hasWon(PlayerType.X)){
            printTable();
            getNextStep();
            getAINextMove();
        }
                
    }
    
    public void getAINextMove() throws CellException{
        SimplePlayer sp = new SimplePlayer(PlayerType.O);
        board.put(sp.nextMove(board));
    }
    
    public void getNextStep() throws CellException{
        System.out.println("Te lépsz!(X)\n");
        Scanner sc = new Scanner(System.in);
        int n, m;
        System.out.println("Sor: ");
        n = sc.nextInt();
        System.out.println("Oszlop: ");
        m = sc.nextInt();
        Cell cell = new Cell(n,m,PlayerType.X);
        board.put(cell);
        
    }
    
    public void printTable() throws CellException{
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(board.getCell(i, j).equals(PlayerType.EMPTY)){
                    System.out.print("E");
                }else{
                    System.out.print(board.getCell(i, j));
                }
                if(j < 2 ){
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }
}
